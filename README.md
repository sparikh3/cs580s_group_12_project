Project Configurations :

Project is build using Eclipse - Android development environment.

Minimum API require on the device to run the project : 14
Target API version : Android 17
Application is Compiled using SDK version : 17

Implemented Sensors:
Camera to detect the face Object from the frame
Accelerometer to detect the shake of phone

References :

https://stackoverflow.com/questions/5271448/how-to-detect-shake-event-with-android
https://developer.android.com/reference/android/hardware/Camera
https://developer.android.com/guide/topics/sensors/sensors_overview
https://github.com/safetysystemtechnology/android-shake-detector
http://ieeexplore.ieee.org/xpl/articleDetails.jsp?arnumber=6825217
http://jasonmcreynolds.com
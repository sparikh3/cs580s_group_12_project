package com.tec.fontsize.measurement;

public class PlotPoints {

	private final float eye_Dist;

	public PlotPoints(final float eyeDistance, final float deviceDistance) {
		eye_Dist = eyeDistance;
	}
	public float getEye_Distance() {
		return eye_Dist;
	}
}

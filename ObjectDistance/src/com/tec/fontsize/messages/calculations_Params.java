package com.tec.fontsize.messages;

public class calculations_Params {

	private float currentAvgObj_Distance;
	private float confidence;
	private float distToObj;
	private float ptime_LastFrame;
	private float eyes_Distance;
	private int calcRem;

	public float getcurrentAvgObj_Distance() {
		return currentAvgObj_Distance;
	}

	public void setcurrentAvgObj_Distance(final float currentAvgObj_DistanceIn) {
		currentAvgObj_Distance = currentAvgObj_DistanceIn;
	}

	public float getConfidence() {
		return confidence;
	}

	public void setConfidence(final float confidenceIn) {
		confidence = confidenceIn;
	}

	public float getDistToObj() {
		return distToObj;
	}

	public void setDistToObj(final float distToFace) {
		distToObj = distToFace;
	}

	public float getptime_LastFrame() {
		return ptime_LastFrame;
	}

	public void setptime_LastFrame(final float ptime_LastFrameIn) {
		ptime_LastFrame = ptime_LastFrameIn;
	}

	public float getEyes_Distance() {
		return eyes_Distance;
	}

	public void setEyes_Distance(final float eyes_DistanceIn) {
		eyes_Distance= eyes_DistanceIn;
	}

	public int getcalcRem() {
		return calcRem;
	}

	public void setcalcRem(final int calcRemIn) {
		calcRem = calcRemIn;
	}
}

package com.tec.fontsize.messages;

import java.util.ArrayList;
import java.util.List;

public class MessageList {

	public static final int MEASUREMENT_STEP = 1;
	public static final int DONE_CALIBRATION = 2;
	
	private static MessageList myInstance = new MessageList();

	public static MessageList getObj() {
		return myInstance;
	}

	private final List<MessageListener> listenerList = new ArrayList<MessageListener>();

	public boolean registerListener(final MessageListener listener) {
		if (!listenerList.contains(listener)) {
			return listenerList.add(listener);
		}
		return false;
	}

	public boolean unregisterListener(final MessageListener listener) {
		return listenerList.remove(listener);
	}

	public void sendMessage(final int messageID, final Object message) {
		for (MessageListener ml : listenerList) {
			ml.onMessage(messageID, message);
		}
	}
}

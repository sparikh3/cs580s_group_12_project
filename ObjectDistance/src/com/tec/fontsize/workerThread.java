package com.tec.fontsize;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera.Size;
import android.media.FaceDetector;
import android.media.FaceDetector.Face;
import android.util.Log;

public class workerThread extends Thread {

	public static final String FACEDETECTIONTHREAD_TAG = "FaceDetectionThread";

	private Face myFace;
	private final byte[] byteData;
	private final Size mySize;
	private Bitmap currentFrame;

	public workerThread(final byte[] myDataIn, final Size previewSizeIn) {
		byteData = myDataIn;
		mySize = previewSizeIn;
	}

	public Face getCurrentObject() {
		return myFace;
	}

	public Bitmap setCurrentObject() {
		return currentFrame;
	}

	@Override
	public void run() {

		long t = System.currentTimeMillis();

		YuvImage myImage = new YuvImage(byteData, ImageFormat.NV21,mySize.width, mySize.height, null);
		ByteArrayOutputStream byteFrame = new ByteArrayOutputStream();

		if (!myImage.compressToJpeg(new Rect(0, 0, mySize.width,mySize.height), 100, byteFrame)) {
			Log.e("Camera Photo compress", "compressToJpeg failed");
		}

		Log.i("Timing", "Compression finished: "+ (System.currentTimeMillis() - t));
		t = System.currentTimeMillis();

		BitmapFactory.Options bitmapfactory = new BitmapFactory.Options();
		bitmapfactory.inPreferredConfig = Bitmap.Config.RGB_565;

		currentFrame = BitmapFactory.decodeStream(new ByteArrayInputStream(byteFrame.toByteArray()), null, bitmapfactory);

		Log.i("Timing", "Decode Finished: " + (System.currentTimeMillis() - t));
		t = System.currentTimeMillis();

		Matrix matrix = new Matrix();
		matrix.postRotate(90);
		matrix.preScale(-1, 1);
		currentFrame = Bitmap.createBitmap(currentFrame, 0, 0,mySize.width, mySize.height, matrix, false);

		Log.i("Timing",	"Rotate, Create finished: " + (System.currentTimeMillis() - t));
		t = System.currentTimeMillis();

		if (currentFrame == null) {
			Log.e(FACEDETECTIONTHREAD_TAG, "Could not decode Image");
			return;
		}

		FaceDetector fd = new FaceDetector(currentFrame.getWidth(),currentFrame.getHeight(), 1);

		Face[] myfacesArray = new Face[1];
		fd.findFaces(currentFrame, myfacesArray);

		Log.i("Timing","FaceDetection finished: " + (System.currentTimeMillis() - t));
		t = System.currentTimeMillis();

		myFace = myfacesArray[0];
		Log.d(FACEDETECTIONTHREAD_TAG, "Found: " + myfacesArray[0] + " Faces");
	}
}

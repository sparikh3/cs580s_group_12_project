package com.tec.fontsize;

import java.text.DecimalFormat;
import java.util.List;

import com.tec.fontsize.messages.calculations_Params;
import com.tec.fontsize.ShakeDetector.OnShakeListener;
import com.tec.fontsize.R;
import com.tec.fontsize.messages.MessageList;
import com.tec.fontsize.messages.MessageListener;

import android.app.Activity;
import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MainActivity extends Activity implements MessageListener {

	private SensorManager SensorManager;
	private Sensor Accelerometer;
	private ShakeDetector ShakeDetector;
	private CameraSurfaceView camView;
	Camera myCam;

	private final static DecimalFormat _decimalFormater = new DecimalFormat("0.0");

	private int camHeight;
	private int camWidth;
	TextView displayDistance;
	Button calibrateButton;

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.measurement_activity);

		camView = (CameraSurfaceView) findViewById(R.id.surface_camera);

		RelativeLayout.LayoutParams layout = new RelativeLayout.LayoutParams( (int) (0.95 * this.getResources().getDisplayMetrics().widthPixels),(int) (0.6 * this.getResources().getDisplayMetrics().heightPixels));

		layout.setMargins(0, (int) (0.05 * this.getResources().getDisplayMetrics().heightPixels), 0, 0);

		camView.setLayoutParams(layout);
		displayDistance = (TextView) findViewById(R.id.currentDistance);
		calibrateButton = (Button) findViewById(R.id.calibrateButton);

		SensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		Accelerometer = SensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		ShakeDetector = new ShakeDetector();
		ShakeDetector.setOnShakeListener(new OnShakeListener() {
			@Override
			public void onShake(int count) {
				android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(1);
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		SensorManager.registerListener(ShakeDetector, Accelerometer,	SensorManager.SENSOR_DELAY_UI);
		MessageList.getObj().registerListener(this);

		myCam = Camera.open(1);
		Camera.Parameters param = myCam.getParameters();

		List<Size> pSize = param.getSupportedPictureSizes();
		double deviceRatio = (double) this.getResources().getDisplayMetrics().widthPixels
				/ (double) this.getResources().getDisplayMetrics().heightPixels;

		Size bestSize = pSize.get(0);
		double bestRation = (double) bestSize.width / (double) bestSize.height;

		for (Size size : pSize) {
			double sizeRatio = (double) size.width / (double) size.height;

			if (Math.abs(deviceRatio - bestRation) > Math.abs(deviceRatio
					- sizeRatio)) {
				bestSize = size;
				bestRation = sizeRatio;
			}
		}
		camHeight = bestSize.height;
		camWidth = bestSize.width;

		Log.d("PInfo", camWidth + " x " + camHeight);

		param.setPreviewSize(camWidth, camHeight);
		myCam.setParameters(param);

		camView.setCamera(myCam);
	}

	@Override
	protected void onPause() {
		SensorManager.unregisterListener(ShakeDetector);
		super.onPause();
		MessageList.getObj().unregisterListener(this);
		resetCam();
	}

	public void pressedCalibrate(final View v) {

		if (!camView.isCalibrated()) {

			calibrateButton.setBackgroundResource(R.drawable.yellow_button);
			camView.calibrate();
		}
	}

	public void pressedReset(final View v) {

		if (camView.isCalibrated()) {

			calibrateButton.setBackgroundResource(R.drawable.red_button);
			camView.reset();
		}
	}

	public void onShowMiddlePoint(final View view) {
		boolean on = true;
		camView.showMiddleEye(on);
	}

//	public void onShowEyePoints(final View view) {
//		boolean on = false;
//		camView.showEyePoints(on);
//	}

	public void updateUI(final calculations_Params message) {

		displayDistance.setText(_decimalFormater.format(message
				.getDistToObj()) + " cm");

		float fontRatio = message.getDistToObj() / 29.7f;

		displayDistance.setTextSize(fontRatio * 20);

	}

	private void resetCam() {
		camView.reset();

		myCam.stopPreview();
		myCam.setPreviewCallback(null);
		myCam.release();
	}

	@Override
	public void onMessage(final int messageIDIn, final Object message) {

		switch (messageIDIn) {

		case MessageList.MEASUREMENT_STEP:
			updateUI((calculations_Params) message);
			break;
		case MessageList.DONE_CALIBRATION:
			calibrateButton.setBackgroundResource(R.drawable.green_button);
			break;
		default:
			break;
		}

	}
}
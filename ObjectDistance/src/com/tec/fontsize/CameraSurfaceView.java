package com.tec.fontsize;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.Size;
import android.media.FaceDetector;
import android.media.FaceDetector.Face;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;

import com.tec.fontsize.measurement.PlotPoints;
import com.tec.fontsize.messages.calculations_Params;
import com.tec.fontsize.messages.MessageList;

public class CameraSurfaceView extends SurfaceView implements Callback,	Camera.PreviewCallback {

	public static final int CALIBRATION_DISTANCE_A4_MM = 294;
	public static final int CALIBRATION_MEASUREMENTS = 10;
	public static final int AVERAGE_THREASHHOLD = 5;
	private float distanceToCalibration = -1;
	private float AvgObjectDistance = -1;
	private float distanceToObject = -1;
	private final SurfaceHolder mHolder;
	private Camera mCamera;
	private Face foundObject = null;
	private int intThreshold = CALIBRATION_MEASUREMENTS;
	private workerThread workerThread;
	private List<PlotPoints> plotpoints;
	protected final Paint midPoint = new Paint();

	private Size mySize;

	private boolean isCalibrated = false;
	private boolean _calibrating = false;
	private int calibrationRem = -1;

	public CameraSurfaceView(final Context context, final AttributeSet attrs) {
		super(context, attrs);
		midPoint.setARGB(100, 200, 0, 0);
		midPoint.setStyle(Paint.Style.FILL);
		midPoint.setStrokeWidth(2);


		mHolder = getHolder();
		mHolder.addCallback(this);
	}

	public void setCamera(final Camera camera) {
		mCamera = camera;

		if (mCamera != null) {
			requestLayout();

			Camera.Parameters params = mCamera.getParameters();
			camera.setDisplayOrientation(90);
			List<String> focusModes = params.getSupportedFocusModes();
			if (focusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
				// set the focus mode
				params.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
				// set Camera parameters
				mCamera.setParameters(params);
			}
		}
	}

	/**
	 * Variables for the onDraw method, in order to prevent variable allocation
	 * to slow down the sometimes heavily called onDraw method
	 */
	private final PointF _middlePoint = new PointF();
	private final Rect _trackingRectangle = new Rect();

	private final static int RECTANGLE_SIZE = 20;
	private boolean _showTracking = true;

	@SuppressLint("DrawAllocation")
	@Override
	protected void onDraw(final Canvas canvas) {

		if (foundObject != null) {

			foundObject.getMidPoint(_middlePoint);

			Log.i("Camera", _middlePoint.x + " : " + _middlePoint.y);

			// portrait mode!
			float heightRatio = canvas.getHeight() / (float) mySize.width;
			float widthRatio = canvas.getWidth() / (float) mySize.height;

			Log.i("Drawcall", _middlePoint.x + " : " + _middlePoint.y);

			int realX = (int) (_middlePoint.x * widthRatio);
			int realY = (int) (_middlePoint.y * heightRatio);

			Log.i("Drawcall", "Real :" + realX + " : " + realY);

			if (_showTracking) {
				// Middle point
				_trackingRectangle.left = realX - RECTANGLE_SIZE;
				_trackingRectangle.top = realY - RECTANGLE_SIZE;
				_trackingRectangle.right = realX + RECTANGLE_SIZE;
				_trackingRectangle.bottom = realY + RECTANGLE_SIZE;
				canvas.drawRect(_trackingRectangle, midPoint);
			}
		}
	}

	public void reset() {
		distanceToCalibration = -1;
		AvgObjectDistance = -1;
		isCalibrated = false;
		_calibrating = false;
		calibrationRem = -1;
	}

	public void calibrate() {
		if (!_calibrating || !isCalibrated) {
			plotpoints = new ArrayList<PlotPoints>();
			_calibrating = true;
			calibrationRem = CALIBRATION_MEASUREMENTS;
			intThreshold = CALIBRATION_MEASUREMENTS;
		}
	}

	private void doneCalibrating() {
		isCalibrated = true;
		_calibrating = false;
		workerThread = null;
		// _measurementStartet = false;

		intThreshold = AVERAGE_THREASHHOLD;

		distanceToCalibration = AvgObjectDistance;
		MessageList.getObj().sendMessage(MessageList.DONE_CALIBRATION, null);
	}

	public boolean isCalibrated() {
		return isCalibrated || _calibrating;
	}

	public void showMiddleEye(final boolean on) {
		_showTracking = on;
	}



	private void updateMeasurement(final FaceDetector.Face currentFace) {
		if (currentFace == null) {
			return;
		}

		foundObject = workerThread.getCurrentObject();

		plotpoints.add(new PlotPoints(foundObject.eyesDistance(), CALIBRATION_DISTANCE_A4_MM * (distanceToCalibration / foundObject.eyesDistance())));

		while (plotpoints.size() > intThreshold) {
			plotpoints.remove(0);
		}

		float sum = 0;
		for (PlotPoints p : plotpoints) {
			sum += p.getEye_Distance();
		}

		AvgObjectDistance = sum / plotpoints.size();

		distanceToObject = CALIBRATION_DISTANCE_A4_MM* (distanceToCalibration / AvgObjectDistance);

		distanceToObject = MM_TO_CM(distanceToObject);

		calculations_Params message = new calculations_Params();
		message.setConfidence(currentFace.confidence());
		message.setcurrentAvgObj_Distance(AvgObjectDistance);
		message.setDistToObj(distanceToObject);
		message.setEyes_Distance(currentFace.eyesDistance());
		message.setcalcRem(calibrationRem);
		message.setptime_LastFrame(_processTimeForLastFrame);

		MessageList.getObj().sendMessage(MessageList.MEASUREMENT_STEP, message);
	}

	private long _lastFrameStart = System.currentTimeMillis();
	private float _processTimeForLastFrame = -1;

	@Override
	public void onPreviewFrame(final byte[] data, final Camera camera) {
		if (calibrationRem == -1)
			return;

		if (calibrationRem > 0) {
			// Doing calibration !

			if (workerThread != null && workerThread.isAlive()) {
				// Drop Frame
				return;
			}

			// No face detection started or already finished
			_processTimeForLastFrame = System.currentTimeMillis() - _lastFrameStart;
			_lastFrameStart = System.currentTimeMillis();

			if (workerThread != null) {
				calibrationRem--;
				updateMeasurement(workerThread.getCurrentObject());

				if (calibrationRem == 0) {
					doneCalibrating();

					invalidate();
					return;
				}
			}

			workerThread = new workerThread(data,mySize);
			workerThread.start();

			invalidate();
		} else {
			// Simple Measurement

			if (workerThread != null
					&& workerThread.isAlive()) {
				// Drop Frame
				return;
			}

			// No face detection started or already finished
			_processTimeForLastFrame = System.currentTimeMillis()
					- _lastFrameStart;
			_lastFrameStart = System.currentTimeMillis();

			if (workerThread != null)
				updateMeasurement(workerThread.getCurrentObject());

			workerThread = new workerThread(data,
					mySize);
			workerThread.start();
			invalidate();
		}
	}

	@Override
	public void surfaceCreated(final SurfaceHolder holder) {
		synchronized (this) {
			// This allows us to make our own drawBitmap
			this.setWillNotDraw(false);
		}
	}

	@Override
	public void surfaceDestroyed(final SurfaceHolder holder) {
		mCamera.release();
		mCamera = null;
	}

	@Override
	public void surfaceChanged(final SurfaceHolder holder, final int format,
			final int width, final int height) {

		if (mHolder.getSurface() == null) {
			// preview surface does not exist
			return;
		}

		// stop preview before making changes
		try {
			mCamera.stopPreview();
		} catch (Exception e) {
			// ignore: tried to stop a non-existent preview
		}

		Parameters parameters = mCamera.getParameters();
		mySize = parameters.getPreviewSize();
		 mCamera.setDisplayOrientation(90);
		 mCamera.setParameters(parameters);

		// start preview with new settings
		try {
			mCamera.setPreviewDisplay(mHolder);
			mCamera.startPreview();
			mCamera.setPreviewCallback(this);

		} catch (Exception e) {
			Log.d("This", "Error starting camera preview: " + e.getMessage());
		}
	}
	
	public float MM_TO_CM(final float mm) {
		return mm / 10;
	}

	public float MM_TO_CM(final int mm) {
		return mm / 10.0f;
	}
	
}
